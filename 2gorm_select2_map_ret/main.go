package main

import (
	"fmt"
	"github.com/gookit/goutil/dump"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"store_appoint_keys_to_redis/store_data_to_redis"
	"time"
)

type User struct {
	ID    int    `json:"id" gorm:"primaryKey;column:id" `
	Name  string `json:"name" gorm:"column:name"`
	Email string `json:"email" gorm:"column:email"`
	// Notice 使用map接收GORM的结果时 map的key是gorm定义的内容，存到redis中也是 phone_number，需要json标签跟gorm标签对应！
	PhoneNumber string    `json:"phone_number" gorm:"phone_number"`
	CreateAt    time.Time `json:"createAt" gorm:"column:create_at;default:null"`
	UpdateAt    time.Time `json:"updateAt" gorm:"column:update_at;default:null"`
}

func main() {
	dsn := "root:123@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	fmt.Println("db: ", db)

	// Notice 假如实际中 create_at、update_at 这2个字段没有用处，直接不查询这2个字段
	// Notice 将查询的结果放到map中
	// Notice 但是实际中绝大多数的情况都会把查询结果放到结构体中方便取值！这种方法在实际中不太好用
	result := make([]map[string]interface{}, 0)

	err = db.Table("user").Select("id", "name", "email", "phone_number").Find(&result).Error
	if err != nil {
		panic(err)
	}

	dump.P(result) // 数据库中有2条数据，Notice 注意 phone_number 跟gorm标签对应！需要json标签与gorm标签写成一样的
	/*
		[]map[string]interface {} [ #len=2
		  map[string]interface {} { #len=4
		    "id": uint32(1),
		    "name": string("naruto"), #len=6
		    "email": string("123@qq.com"), #len=10
		    "phone_number": string("13333333333"), #len=11
		  },
		  map[string]interface {} { #len=4
		    "id": uint32(2),
		    "name": string("sasuke"), #len=6
		    "email": string("222@qq.com"), #len=10
		    "phone_number": string("16666666666"), #len=11
		  },
		]
	*/

	// 然后使用pipeline写入到redis中即可
	// 最后使用pipeline写入到redis中即可
	errStore := store_data_to_redis.StoreUsersToRedis(result)
	if errStore != nil {
		fmt.Println("errStore: ", errStore)
	} else {
		fmt.Println("用户数据保存到redis成功!!!")
	}
}
