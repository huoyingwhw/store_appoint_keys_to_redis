module store_appoint_keys_to_redis

go 1.18

require (
	github.com/fatih/structs v1.1.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gookit/goutil v0.6.9
	github.com/json-iterator/go v1.1.12
	gorm.io/driver/mysql v1.5.1
	gorm.io/gorm v1.25.1
)

require (
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/gookit/color v1.5.3 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.8 // indirect
	github.com/xo/terminfo v0.0.0-20220910002029-abceb7e1c41e // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
