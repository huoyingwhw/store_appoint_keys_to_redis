CREATE TABLE `user`
(
    `id`           int unsigned NOT NULL AUTO_INCREMENT,
    `name`         varchar(20) COLLATE utf8mb4_general_ci                        NOT NULL DEFAULT '',
    `email`        varchar(255) COLLATE utf8mb4_general_ci                       NOT NULL DEFAULT '',
    `phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
    `create_at`    datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_at`    datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;